function addNumbers() {
 let num1 = Number(document.getElementById('num1').value);
 let num2 = Number(document.getElementById('num2').value);

 document.getElementById('result').innerHTML = num1 + num2;
 return false;
}

document.getElementById('add').addEventListener('click', addNumbers);
function subNumbers() {
 let num1 = Number(document.getElementById('num1').value);
 let num2 = Number(document.getElementById('num2').value);

 document.getElementById('result').innerHTML = num1 - num2;
 return false;
}
document.getElementById('sub').addEventListener('click', subNumbers);
function mulNumbers() {
 let num1 = Number(document.getElementById('num1').value);
 let num2 = Number(document.getElementById('num2').value);

 document.getElementById('result').innerHTML = num1 * num2;
 return false;
}
document.getElementById('mul').addEventListener('click', mulNumbers);

function divNumbers() {
 let num1 = Number(document.getElementById('num1').value);
 let num2 = Number(document.getElementById('num2').value);

 document.getElementById('result').innerHTML = num1 / num2;
 return false;
}

document.getElementById('div').addEventListener('click', divNumbers);
function modNumbers() {
     let num1 = Number(document.getElementById('num1').value);
     let num2 = Number(document.getElementById('num2').value);

     document.getElementById('result').innerHTML = num1 % num2;
     return false;
 }

 document.getElementById('mod').addEventListener('click', modNumbers);

function clearTxt(){
   document.getElementById('num1').value = '';
   document.getElementById('num2').value = '';
   document.getElementById('result').innerHTML = '';
}
 document.getElementById('clear').addEventListener('click', clearTxt);
